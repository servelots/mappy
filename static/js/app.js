
$(document).ready(function () {
  // initialize mapbox
  var map = initMapbox();

  //fetch csv data
  fetchCSV(config.csvPath)
    .then(data => makeGeoJSON(data))
    .catch(err => console.log(err))

  function makeGeoJSON(csvData) {
    csv2geojson.csv2geojson(csvData, {
      latfield: config.latField,
      longfield: config.longField,
      delimiter: ','
    }, function (err, data) {
      addDataLayer(map, data);
      var bbox = turf.bbox(data);
      map.fitBounds(bbox, { padding: 50 });
    });
  };
  
});

//Fetch and return csv data
async function fetchCSV (url) {
  var response = await fetch(url);
  var data = await response.text();
  return data;
}

// Create Mapbox config and bind events
var initMapbox = () => {

   var transformRequest = (url, resourceType) => {
     var isMapboxRequest =
       url.slice(8, 22) === "api.mapbox.com" ||
       url.slice(10, 26) === "tiles.mapbox.com";
     return {
       url: isMapboxRequest
         ? url.replace("?", "?pluginName=sheetMapper&")
         : url
     };
   };

  //YOUR TURN: add your Mapbox token

  mapboxgl.accessToken = config.accessToken; //Mapbox token 
  var map = new mapboxgl.Map({
    container: 'map', // container id
    style: {
      "version": 8,
      "sources": {
        "simple-tiles": {
          "type": "raster",
          // point to our third-party tiles. Note that some examples
          // show a "url" property. This only applies to tilesets with
          // corresponding TileJSON (such as mapbox tiles). 
          "tiles": config.rasterTilesPath,
          "tileSize": 256
        }
      },
      "layers": [{
        "id": "simple-tiles",
        "type": "raster",
        "source": "simple-tiles",
        "minzoom": config.minzoom,
        "maxzoom": config.maxzoom
      }]
    },
    //style: 'mapbox://styles/mapbox/streets-v11', // YOUR TURN: choose a style: https://docs.mapbox.com/api/maps/#styles
    center: config.center, // starting position [lng, lat]
    zoom: config.zoom,// starting zoom

    transformRequest: transformRequest
  });
  // add zoom control widget
  map.addControl(new mapboxgl.NavigationControl());



  map.on('load', function () {
    
    // Bind events
    // Change the cursor to a pointer when the mouse is over the places layer.
    map.on('mouseenter', 'csvData', function () {
      map.getCanvas().style.cursor = 'pointer';
    });

    // Change it back to a pointer when it leaves.
    map.on('mouseleave', 'places', function () {
      map.getCanvas().style.cursor = '';
    });
    // When a click event occurs on a feature in the csvData layer, open a popup at the
    // location of the feature, with description HTML from its properties.
    map.on('click', 'csvData', function (e) {
      var coordinates = e.features[0].geometry.coordinates.slice();
      currentEvent = e.features[0].properties;
      //set popup text
      //You can adjust the values of the popup to match the headers of your CSV.
      // For example: e.features[0].properties.Name is retrieving information from the field Name in the original CSV.

      var popupHtml = `<h3>` + e.features[0].properties[config.titleField] + 
                        `</h3>` + 
                        `<p>` + `<b>` + `Notes: ` + 
                          `</b>` + e.features[0].properties[config.notesField] + 
                        `</p>` + 
                        btn('images', currentEvent[config.imgField], 'Images') +  
                        btn('video', currentEvent[config.vidField], 'Video') + 
                        btn('audio', currentEvent[config.audioField], 'Audio');

      // Ensure that if the map is zoomed out such that multiple
      // copies of the feature are visible, the popup appears
      // over the copy being pointed to.
      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }

      //add Popup to map
      new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML(popupHtml)
        .addTo(map);

    });


  });

  return map;
}

// Add feature layer on the map
var addDataLayer = (map, data) => {
    map.addLayer({
      'id': 'csvData',
      'type': 'circle',
      'source': {
        'type': 'geojson',
        'data': data
      },
      'paint': {
        'circle-radius': 7,
        'circle-color': 'purple',
        "circle-stroke-color": "#ffffFF",
        "circle-stroke-width": 1,
        "circle-stroke-opacity": 0.5
      }
    });
}

//Popup btn to trigger modal
var btn = (type, key, btnTxt) => {
  return `<a href="#openModal" class="modalButton" 
          onclick="showModal('${type}', '${key}', '${btnTxt}')"> 
          ${btnTxt} </a>`
}

//Create modal per type
function showModal(type, src, title) {
  console.log(type, src, title)
  var modalHeaderText = document.getElementById('modalTitle');
  modalHeaderText.innerText = title;

  if (type === 'images') {
    var imgUrl = src.split(';');
    var imgElement = "";
    var iframBox = document.getElementById('imgBox');
    iframBox.innerHTML = '';
    imgUrl.forEach((element, id) => {
    var fullImage = document.createElement('a');
    fullImage.setAttribute("target","_blank")
    fullImage.href = element;
    fullImage.className = 'photoRefernce'
      imgElement = document.createElement('img');
      imgElement.className = "photo";
      imgElement.src = element;
      fullImage.append(imgElement);
      iframBox.append(fullImage);
    })
  }
  else if (type === 'panarom' && src) {
    var iframBox = document.getElementById('imgBox');
    iframBox.innerHTML = '';
    const iframe = document.createElement('iframe');
    iframe.src = src;
    iframe.setAttribute('allowFullScreen', '');
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('scrolling', 'no');
    document.getElementById('imgBox').appendChild(iframe);
  }
  else if (type === 'video' || type === 'audio') {
    var iframBox = document.getElementById('imgBox');
    iframBox.innerHTML = '';
    var video = document.createElement('video');
    video.src = src;
    video.type ='video/mp4';
    video.setAttribute('width', '750');
    video.setAttribute('height', '450');
    video.setAttribute('class', 'vid');
    video.controls = true;
    document.getElementById('imgBox').appendChild(video);
  }
  /*else if (type === 'audio' && src) {
    var iframBox = document.getElementById('imgBox');
    iframBox.innerHTML = '';
    var audio = document.createElement('audio');
    audio.src = src;
    audio.setAttribute('type','audio/mpeg');
    audio.setAttribute('class', 'audio');
    audio.controls = true;
    iframBox.append(audio);
  }*/
}

