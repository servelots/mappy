var config = {
	//Map configuration
	accessToken: 'pk.eyJ1IjoibWFuaWthbnRhbnZhIiwiYSI6ImNrbWVtcXFxMzJ3bTgyeG42Z2d1czNxOHEifQ.gX9UfLWNQTo5yKM4sugHRw',
	rasterTilesPath: [
            "./static/tiles/{z}/{x}/{y}.jpeg",
            "./static/tiles/{z}/{x}/{y}.jpeg",
			],
	minzoom: 14,
	maxzoom: 18,
	// starting zoom
	zoom: 5,
	// starting position [lng, lat]
	center: [78.820, 25.426],
	//CSV file path
	csvPath: './static/csv/form-1__da-stories.csv',
	//CSV header name configuration
	latField: 'lat_2_Pin_location',
	longField: 'long_2_Pin_location',
	titleField: '1_Name',
	notesField: '3_Details',
	imgField: '5_Images',
	vidField: '7_Video',
	audioField: '6_Audio'
}