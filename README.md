# Mappy
A map portal with input data from a CSV file

## setup

* Clone this project `git clone https://gitlab.com/servelots/mappy.git`
* Head over to `static/js/config.js` and edit the config object.
* setup nginx or apache config with `index.html` as document root


```javascript

var config = {
	//Map configuration
	accessToken: 'mapbox access token',
	rasterTilesPath: [
            "./static/tiles/{z}/{x}/{y}.jpeg",
            "./static/tiles/{z}/{x}/{y}.jpeg",
			],
	minzoom: 14,
	maxzoom: 18,
	// starting zoom
	zoom: 5,
	// starting position [lng, lat]
	center: [78.820, 25.426 ],
	//CSV file path
	csvPath: './static/csv/form-1__da-stories.csv',
	//CSV header configuration
	latField: 'lat_2_Pin_location',
	longField: 'long_2_Pin_location',
	imgField: '5_Images',
	vidField: '7_Video',
	audioField: '6_Audio'
}
```

### Url downloader script
context: Exported csv from Epicollect will give media columns with remote url

In case if your csv sheet has remote urls and you need to download 
locally, the run the python script `scripts/export_url_download.py`
The downloaded files will be saved to the path `static/media`

```python
/usr/bin/python export_url_download.py <csv_file_path> <column_header_name>
```
### Update local media path in the excel sheet
Run excel formula to find a substrting `=SUBSTRING(col_name, start_index, end_index)`
