#!/usr/bin/python

import os
import sys
import urllib
import csv

try:
	csv_filepath = sys.argv[1]
	url_name = sys.argv[2]
except:
	print "\nERROR: Please specify filename and url column name to download\n"	
	print "Usage:"
	print " $ export_url_download.py path_to_file.csv media_column_name\n"
	print "- First param should be the csv file path"
	print "- Second param should be the column name that has image urls to download\n"	
	sys.exit(0)	

current_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
# rel_path = "../static/csv/form-1__da-stories.csv"
abs_file_path = os.path.join(current_dir, csv_filepath)
save_to_path = "../static/media"
abs_save_to_path = os.path.join(current_dir, save_to_path)

# open csv file to read
with open(abs_file_path, 'r') as csvfile:
	csv_reader = csv.reader(csvfile)
	# iterate on all rows in csv
	for row_index,row in enumerate(csv_reader):
		# find the url column name to download in first row
		if row_index == 0:
			IMAGE_URL_COL_NUM = None
			for col_index,col in enumerate(row):
				if col == url_name:
					IMAGE_URL_COL_NUM = col_index 				
			if IMAGE_URL_COL_NUM is None:
				print "\nERROR: url column name '"+url_name+"' not found, available options:"	
				for col_index,col in enumerate(row):
					print " " + col	
				print "\nUsage:"
				print " $ picodash_export_url_download.py data.csv image_url\n"
				sys.exit(0)
			continue
		# check if we have an image URL and download in rows > 1
		image_urls = row[IMAGE_URL_COL_NUM]
		image_urls = image_urls.split(';')
		#print image_urls
		for image_url in image_urls:
			if image_url != '' and image_url != "\n":
				# Find index of name param and add 5 to includes chars name= in url
				start_index = image_url.find('name')+5 
				end_index = len(image_url)
				s = slice(start_index, end_index, 1)
				image_filename = image_url[s] 
				directory = abs_save_to_path 
				if not os.path.exists(directory):
					os.makedirs(directory)
				try:
					urllib.urlretrieve(image_url, directory+'/'+image_filename)
					print "["+str(row_index)+"] Image saved: " + image_filename
				except:
					# second attempt to download if failed
					try:
						urllib.urlretrieve(image_url, directory+'/'+image_filename)
						print "["+str(row_index)+"] Image saved: " + image_filename
					except:
						print "["+str(row_index)+"] Could not download url: " + image_url
			else:
				print "["+str(row_index)+"] No " + url_name